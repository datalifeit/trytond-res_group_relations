# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from .group import GroupGroup, Group, ModelAccess, ModelFieldAccess


def register():
    Pool.register(
        GroupGroup,
        Group,
        ModelAccess,
        ModelFieldAccess,
        module='res_group_relations', type_='model')